var player;
var canvas = new Canvas(fixedWidth, fixedHeight);
var endGame = false;

var sound = new Sound();

canvas.verticallyCenter();

// check if out dimensions are less than the screens
if (!checkScreenDimensions()) {
  alert("Sorry, please play in a larger device.");
}
 
window.addEventListener("resize", () => {
  canvas.verticallyCenter();
  if (!checkScreenDimensions()) {
    alert("Sorry, please play in a larger device.");
  } 
});

var levelCompleteEvents = Array.from({length: level.length}, (v, i) => new CustomEvent("level-complete", { detail: i }));

function showStart(num, instructions = []) {
  canvas.clear();
  let startOverlay = document.createElement("DIV");
  startOverlay.classList.add("start-overlay");
  setTimeout(() => {
    startOverlay.style.opacity = 1;
  }, 20);
  canvas.el.appendChild(startOverlay);

  let heading = document.createElement("H1");
  heading.innerText = `Level ${num + 1}`;
  heading.style.fontSize = "2.5em";
  heading.style.fontFamily= "Kulim Park, sans-serif";
  heading.style.color = "#161616";
  heading.style.fontWeight = 600;
  heading.style.letterSpacing = "2px";
  heading.style.marginBottom = "20px";
  startOverlay.appendChild(heading);
  
  let para = document.createElement("P");
  para.style.fontSize = "1.2em";
  para.style.lineHeight = "25px";
  startOverlay.appendChild(para);

  let ol = document.createElement("OL");
  para.appendChild(ol);

  instructions.forEach(instruction => {
    let li = document.createElement("li");
    li.innerText = instruction;
    ol.appendChild(li);
  });

  let startBtn = document.createElement("DIV");
  startBtn.classList.add("start-btn");
  startOverlay.appendChild(startBtn);
  
  let startText = document.createElement("DIV");
  startText.classList.add("start-text");
  startText.innerText = "Start";
  startBtn.appendChild(startText);
  
  let startSubText = document.createElement("DIV");
  startSubText.classList.add("start-sub-text");
  // startSubText.innerText = `(level ${num + 1})`;
  startBtn.appendChild(startSubText);
  
  startBtn.addEventListener("click", () => {
    level[num].init(canvas, levelCompleteEvents[num]);
  })
}

window.addEventListener("level-complete", e => {
  if ((e.detail + 1) < level.length && !endGame) {
    showStart(e.detail + 1, instructions_all[e.detail + 1]);
  } else {
    sound.stopPattern = true;
    endGame = false;
    showThankyou();
  }
});

function showDataCollection() {
  // reset level data
  level.forEach(lev => {
    lev.statistics = undefined;
  })

  let wholeContainer = document.createElement("DIV");
  wholeContainer.style.display = "flex";
  wholeContainer.style.position = "absolute";
  wholeContainer.style.width  = "100%";
  wholeContainer.style.height = "100%";
  wholeContainer.style.justifyContent = "center";
  wholeContainer.style.alignItems = "center";
  wholeContainer.style.background = "radial-gradient(circle, #F0F0F0, #AAAAAA)";
  wholeContainer.style.transition = "opacity 500ms ease-in-out";
  canvas.el.appendChild(wholeContainer);

  let formContainer = document.createElement("DIV");
  formContainer.style.display = "flex";
  formContainer.style.flexDirection = "column";
  formContainer.style.alignItems = "center";
  wholeContainer.appendChild(formContainer);
  formContainer.style.width = "50%";

  let heading = document.createElement("H1");
  heading.innerText = "Cognitive Game";
  heading.style.fontSize = "3em";
  heading.style.fontFamily= "Kulim Park, sans-serif";
  heading.style.color = "#161616";
  heading.style.fontWeight = 600;
  heading.style.letterSpacing = "2px";
  heading.style.marginBottom = "40px";
  formContainer.appendChild(heading);
  
  let nameEl = document.createElement("INPUT");
  nameEl.placeholder = "Name";
  formContainer.appendChild(nameEl);

  let ageEl = document.createElement("INPUT");
  ageEl.type = "number";
  ageEl.placeholder = "Age";
  ageEl.addEventListener("input", () => {
    if (parseInt(ageEl.value) != ageEl.value || ageEl.value < 0 || ageEl.value > 100) {
      ageEl.style.border = "2px solid #FF0000";
      ageEl.style.boxShadow = "0 0 4px #FF0000";
    } else {    
      ageEl.style.border = "";
      ageEl.style.boxShadow = "";
    }
  });
  formContainer.appendChild(ageEl);

  let genderEl = document.createElement("SELECT");
  let maleEl = document.createElement("option");
  maleEl.value = "male";
  maleEl.innerText = "Male";
  let femaleEl = document.createElement("option");
  femaleEl.value = "female";
  femaleEl.innerText = "Female";
  genderEl.appendChild(maleEl);
  genderEl.appendChild(femaleEl);
  formContainer.appendChild(genderEl);

  let startBtn = document.createElement("DIV");
  startBtn.classList.add("start-btn");
  startBtn.style.height = "50px";
  startBtn.style.marginTop = "40px";
  formContainer.appendChild(startBtn);
  
  let startText = document.createElement("DIV");
  startText.classList.add("start-text");
  startText.innerText = "Go";
  startBtn.appendChild(startText);

  startBtn.addEventListener("click", () => {
    nameEl.style.border = (nameEl.value == "")? "2px solid #FF0000": "";
    nameEl.style.boxShadow = (nameEl.value == "")? "0 0 4px #FF0000": "";
    
    let ageErrPresent = false;

    if (parseInt(ageEl.value) != ageEl.value || ageEl.value == "" || ageEl.value < 0 || ageEl.value > 100) {
      ageEl.style.border = "2px solid #FF0000";
      ageEl.style.boxShadow = "0 0 4px #FF0000";
      ageErrPresent = true;
    } else {    
      ageEl.style.border = "";
      ageEl.style.boxShadow = "";
    }

    if (nameEl.value != "" != "" && ageEl.value != "" && !ageErrPresent) {
      sound.pattern();
      player = { 
        name: nameEl.value,
        age: ageEl.value,
        gender: genderEl.value
      };
      wholeContainer.style.opacity = 0;
      setTimeout(() => {
        showInstructions();
      }, 500);
    }
  });
}

function showInstructions() {
  canvas.clear();
  let wholeContainer = document.createElement("DIV");
  wholeContainer.style.display = "flex";
  wholeContainer.style.position = "absolute";
  wholeContainer.style.width  = "100%";
  wholeContainer.style.height = "100%";
  wholeContainer.style.justifyContent = "center";
  wholeContainer.style.alignItems = "center";
  wholeContainer.style.flexDirection = "column";
  wholeContainer.style.transition = "opacity 500ms ease-in-out";
  wholeContainer.style.opacity = 0;
  setTimeout(() => {
    wholeContainer.style.opacity = 1;
  }, 20);
  wholeContainer.style.fontFamily= "Kulim Park, sans-serif";
  wholeContainer.style.padding = "10px 30px";
  wholeContainer.style.boxSizing = "border-box";
  canvas.el.appendChild(wholeContainer);

  let heading = document.createElement("H1");
  heading.innerText = "Instructions";
  heading.style.fontSize = "3em";
  heading.style.color = "#161616";
  heading.style.fontWeight = 600;
  heading.style.letterSpacing = "2px";
  heading.style.marginBottom = "40px";
  wholeContainer.appendChild(heading);

  let para = document.createElement("P");
  para.innerText = "This game is developed for the data collection for the project work purpose. Please read the instructions before starting the game.";
  para.style.fontSize = "1.2em";
  para.style.lineHeight = "25px";
  wholeContainer.appendChild(para);

  let ol = document.createElement("OL");
  para.appendChild(ol);

  let li = document.createElement("li");
  li.innerText = "Click on the coloured ball and then drag it into the respective bin so that the ball goes into the respective bin.";
  ol.appendChild(li);
  
  li = document.createElement("li");
  li.innerText = "If an incorrect bin is selected, it will be counted as a drop error.";
  ol.appendChild(li);
  
  li = document.createElement("li");
  li.innerText = "The coloured balls will be coming in a moving conveyor. The player must click and drag the balls into the bin before it reaches the right side of your screen. If any balls is missed to be picked up by the player, it will be counted as a missing error.";
  ol.appendChild(li);
  
  li = document.createElement("li");
  li.innerText = "There are five levels in the game and the next level will start as soon as one level is completed.";
  ol.appendChild(li);

  li = document.createElement("li");
  li.innerText = "The bins are the boxes seen at the bottom of the screen.";
  ol.appendChild(li);

  let tempCont = document.createElement("DIV");
  tempCont.style.display = "flex";
  tempCont.style.justifyContent = "space-around";
  tempCont.style.width = "60%";
  wholeContainer.appendChild(tempCont);

  let startBtn = document.createElement("DIV");
  startBtn.classList.add("start-btn");
  startBtn.style.height = "50px";
  startBtn.style.marginTop = "10px";
  tempCont.appendChild(startBtn);
  
  let startText = document.createElement("DIV");
  startText.classList.add("start-text");
  startText.innerText = "From Start";
  startBtn.appendChild(startText);

  startBtn.addEventListener("click", () => {
    wholeContainer.style.opacity = 0;
      setTimeout(() => {
        showStart(0, instructions_all[0]);
      }, 500);
  });

  let levelSelectBtn = document.createElement("DIV");
  levelSelectBtn.classList.add("start-btn");
  levelSelectBtn.style.height = "50px";
  levelSelectBtn.style.marginTop = "10px";
  tempCont.appendChild(levelSelectBtn);
  
  let levelSelectText = document.createElement("DIV");
  levelSelectText.classList.add("start-text");
  levelSelectText.innerText = "Select Level";
  levelSelectBtn.appendChild(levelSelectText);

  levelSelectBtn.addEventListener("click", () => {
    wholeContainer.style.opacity = 0;
      setTimeout(() => {
        showLevelSelector();
      }, 500);
  });
}

function showThankyou() {
  canvas.clear();
  let wholeContainer = document.createElement("DIV");
  wholeContainer.style.display = "flex";
  wholeContainer.style.position = "absolute";
  wholeContainer.style.width  = "100%";
  wholeContainer.style.height = "100%";
  wholeContainer.style.justifyContent = "center";
  wholeContainer.style.alignItems = "center";
  wholeContainer.style.flexDirection = "column";
  wholeContainer.style.transition = "opacity 500ms ease-in-out";
  wholeContainer.style.opacity = 0;
  setTimeout(() => {
    wholeContainer.style.opacity = 1;
  }, 20);
  wholeContainer.style.fontFamily= "Kulim Park, sans-serif";
  wholeContainer.style.padding = "10px 30px";
  wholeContainer.style.boxSizing = "border-box";
  canvas.el.appendChild(wholeContainer);

  let heading = document.createElement("H1");
  heading.innerText = "Thank You";
  heading.style.fontSize = "3em";
  heading.style.color = "#161616";
  heading.style.fontWeight = 600;
  heading.style.letterSpacing = "2px";
  heading.style.marginBottom = "50px";
  wholeContainer.appendChild(heading);

  let startBtn = document.createElement("DIV");
  startBtn.classList.add("start-btn");
  startBtn.style.height = "50px";
  startBtn.style.marginTop = "10px";
  wholeContainer.appendChild(startBtn);
  
  let startText = document.createElement("DIV");
  startText.classList.add("start-text");
  startText.innerText = "Play Again";
  startBtn.appendChild(startText);

  startBtn.addEventListener("click", () => {
    wholeContainer.style.opacity = 0;
      setTimeout(() => {
        showDataCollection();
      }, 500);
  });

  let levels = level.map((lev, i) => {
    return { lname: i + 1, data: lev.statistics };
  });

  levels = levels.filter((lev) => {
    return (lev.data)
  });

  let sendingData = {
    ...player,
    levels
  };
  
  console.log(sendingData);
  sendDataToServer(sendingData);
}

async function sendDataToServer(data) {
  const rawResponse = await fetch('https://cognitive-game-backend.nikhilnathr.com/addData', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  });
  const content = await rawResponse.text();

  console.log(content);
}

function showLevelSelector() {
  canvas.clear();
  let wholeContainer = document.createElement("DIV");
  wholeContainer.style.display = "flex";
  wholeContainer.style.position = "absolute";
  wholeContainer.style.width  = "100%";
  wholeContainer.style.height = "100%";
  wholeContainer.style.justifyContent = "center";
  wholeContainer.style.alignItems = "center";
  wholeContainer.style.flexDirection = "column";
  wholeContainer.style.transition = "opacity 500ms ease-in-out";
  wholeContainer.style.opacity = 0;
  setTimeout(() => {
    wholeContainer.style.opacity = 1;
  }, 20);
  wholeContainer.style.fontFamily= "Kulim Park, sans-serif";
  wholeContainer.style.padding = "10px 30px";
  wholeContainer.style.boxSizing = "border-box";
  canvas.el.appendChild(wholeContainer);

  let heading = document.createElement("H1");
  heading.innerText = "Select a level";
  heading.style.fontSize = "3em";
  heading.style.color = "#161616";
  heading.style.fontWeight = 600;
  heading.style.letterSpacing = "2px";
  heading.style.marginBottom = "50px";
  wholeContainer.appendChild(heading);

  let levelSelector = document.createElement("SELECT");
  levelSelector.classList.add("not-bigger");
  level.forEach((levl, i) => {
    let levlEl = document.createElement("option");
    levlEl.value = i;
    levlEl.innerText = `Level - ${i + 1}`;
    levelSelector.appendChild(levlEl);
  });
  wholeContainer.appendChild(levelSelector);

  let startBtn = document.createElement("DIV");
  startBtn.classList.add("start-btn");
  startBtn.style.height = "50px";
  startBtn.style.marginTop = "10px";
  wholeContainer.appendChild(startBtn);
  
  let startText = document.createElement("DIV");
  startText.classList.add("start-text");
  startText.innerText = "Play";
  startBtn.appendChild(startText);

  startBtn.addEventListener("click", () => {
    wholeContainer.style.opacity = 0;
      setTimeout(() => {
        endGame = true;
        console.log(levelSelector.value);
        showStart(parseInt(levelSelector.value), instructions_all[levelSelector.value]);
      }, 500);
  });
}

showDataCollection();
// dimensions
const fixedWidth = 900;
const fixedHeight = 600;

const ballRadius = 20;
const initialConveryerPosition = 40;
const gapBetweenBallsHorizondal = 80;
const gapBetweenBallsVertical = 20;
const intialOffsetBalls = -ballRadius;

const bucketAndConveyerOffset = 100;
const bucketWidth = 130;
const bucketHeight = 80;
const bucketGap = 20;

// params
const ballVX = 0.7;

// levels
level = []

// instructions
const instructions_all = [
    [],
    [
      "Only the green balls are to be sorted into the green bin at the bottom of the screen.",
      "In this level, there are balls in grey and black colour which should not be picked.",
    ],
    ["This level will be same as level 2 except you will have to sort 2 colours: blue and green while the rest are not to be picked."],
    ["In this level, you need to sort four types of balls. Green, blue, magenta and yellow are to be picked."],
    ["In this level, you need to sort six types of balls. Green, blue, magenta, yellow, red and cyan coloured balls are to be sorted."]
]; 

level[0] = new Level(5, 12, 3);
level[0].randomColors = [ 
  { label: "transparent", color: "#00000000" },
];
level[0].allowedColors = [ 
  { label: "black", color: "#000000"},
];

level[1] = new Level(5, 12, 3);
level[1].randomColors = [
  { label: "black", color: "#000000" },
  { label: "grey", color: "#CCCCCC" },
];
level[1].allowedColors = [
  { label: "green", color: "#44FF33" },
];

level[2] = new Level(5, 12, 3);
level[2].randomColors = [
  { label: "black", color: "#000000" },
  { label: "grey", color: "#CCCCCC" },
];
level[2].allowedColors = [
  { label: "green", color: "#44FF33" },
  { label: "blue", color: "#4433FF" },
];

level[3] = new Level(5, 12, 3);
level[3].randomColors = [
  { label: "black", color: "#000000" },
  { label: "grey", color: "#CCCCCC" },
];
level[3].allowedColors = [
  { label: "green", color: "#44FF33" },
  { label: "blue", color: "#4433FF" },
  { label: "magenta", color: "#FF00FF" },
  { label: "yellow", color: "#EFEF47" },
];

level[4] = new Level(5, 12, 3);
level[4].randomColors = [
  { label: "black", color: "#000000" },
  { label: "grey", color: "#CCCCCC" },
];
level[4].allowedColors = [ 
  { label: "green", color: "#44FF33" },
  { label: "blue", color: "#4433FF" },
  { label: "magenta", color: "#FF00FF" },
  { label: "yellow", color: "#EFEF47" },
  { label: "red", color: "#FF0000" },
  { label: "cyan", color: "#00FFFF" },
];
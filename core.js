class Canvas {
  constructor(width, height) {
    this.el = document.createElement("DIV");
    this.el.classList.add("canvas");
    this.el.style.width = `${width}px`;
    this.el.style.height = `${height}px`;
    this.el.style.position = "relative";
    // this.el.style.transform = `translateY(${(window.innerHeight - fixedHeight) / 2}px)`;
    document.body.prepend(this.el);
  }
  
  verticallyCenter() {
    this.el.style.top = `${(window.innerHeight - fixedHeight) / 2}px`;
  }

  clear() {
    this.el.innerHTML = "";
  }
}

class Bucket {
  constructor(x = 0, y = 0, color = "#000000", parent = document.body, label = "", droppedABall) {
    this.contents = [];

    this.el = document.createElement("DIV");
    this.color = color;
    this.label = label;
    this.x = x;
    this.y = y;

    this.el.style.position = "absolute";
    this.el.style.left = `${x}px`;
    this.el.style.top = `${y}px`;
    this.el.style.width = `${bucketWidth}px`;
    this.el.style.height = `${bucketHeight}px`;
    this.el.style.border = "1px solid #000000";
    this.el.style.display = "flex";
    this.el.style.justifyContent = "center";
    this.el.style.alignItems = "center";
    this.el.style.fontWeight = 1000;
    this.el.style.fontFamily = "sans-serif";
    this.el.style.fontSize = "1.1em";
    this.el.style.userSelect = "none";
    this.el.style.fontFamily = "Kulim Park, sans-serif";
    this.el.style.textShadow = "0 0 2px #000000";
    this.el.style.transition = "background-color 100ms linear, color 100ms linear";
    this.el.style.backgroundColor = this.color;

    this.el.addEventListener("dragenter", e => {
      if (e.target) {
        e.preventDefault();
        e.dataTransfer.dropEffect = "move";
      }
    })

    this.el.addEventListener("dragover", e => {
      e.preventDefault();
    })

    this.el.addEventListener("drop", e => {
      if (e.target) {
        e.preventDefault();
        if (draggedElement) {
          if (draggedElement.label == this.label) {
            sound.collect();
            e.target.style.backgroundColor = "#FFFFFF";
            setTimeout(() => {
              e.target.style.backgroundColor = this.color;
            }, 200);
          } else {
            sound.collectBad();
          }
          draggedEndTime = performance.now();
          draggedElement.actionTime = draggedEndTime - draggedStartTime;
          draggedElement.el.style.display = "none";
          draggedElement.destinationReached = true;
          droppedABall(draggedElement);
          this.contents.push(draggedElement);
          draggedElement.distance = Math.sqrt(Math.pow((this.x + (bucketWidth / 2)) - draggedElement.clickedPosition.x, 2) + Math.pow(this.y - draggedElement.clickedPosition.y, 2));
        } else {
          console.log("bug! bug! bug!");
        }
      }
    });

    parent.appendChild(this.el);
  }

}

class Ball {
  constructor(x = 0, y = 0, r = 10, color = "#000000", parent = document.body, label = "") {
    this.destinationReached = false;

    this.el = document.createElement("DIV");
    this.label = label;
    this.x = x;
    this.y = y;
    this.r = r;
    this.color = color;
    this.reactionTime = 0;
    this.actionTime = 0;
    this.distance = 0;
    this.dropError = 0;
    this.missingError = 1;
    this.updateCoords();
    
    this.el.style.position = "absolute";
    this.el.style.width = `${this.r*2}px`;
    this.el.style.height = `${this.r*2}px`;
    this.el.style.borderRadius = `${this.r}px`;
    this.el.style.backgroundColor = this.color;
    this.el.style.boxShadow = "inset 0 0 10px #000000";
    if (label == "transparent") this.el.style.display = "none";


    this.el.draggable = true;
  
    this.el.addEventListener("dragstart", e => {
      draggedStartTime = performance.now();
      this.reactionTime = draggedStartTime - draggedEndTime;
      e.dataTransfer.dropEffect = 'move';
      draggedElement = this;
      e.target.style.opacity = 0.3;
      this.clickedPosition = { x: this.x + this.r, y: this.y + this.r };
    })
    this.el.addEventListener("dragend", e => {
      if (!this.destinationReached) {
        event.target.style.opacity = '';
        this.actionTime = 0;
      } 
    })

    parent.appendChild(this.el);
  }

  getCoords() {
    return { x: this.x + this.r, y: this.y + this.r, r: this.r };
  }

  updateCoords() {
    this.el.style.top = `${this.y}px`;
    this.el.style.left = `${this.x}px`;
  }
}

class Level {
  constructor(rows = 5, columns = 12, allowedColorsPerColumn = 3) {
    this.allowedColors = [];
    this.randomColors = [];
    this.ballsDropped = [];

    this.rows = rows;
    this.columns = columns;

    this.allowedColorsPerColumn = allowedColorsPerColumn;

    // datas
    this.balls = [];
    this.buckets = [];
    this.ballsInBaskets = [];
    this.data = [];

    this.dropError = 0;
    this.missingError = 0

    this.dropErrors = {};
  }

  init(canvas, completeEvent) {
    this.completeEvent = completeEvent;
    this.canvas = canvas;
    this.reset();
    // this time should only be set after resetting globals
    draggedEndTime = performance.now();
    
    this.drawBalls();
    this.drawBuckets();
    this.play();
  }

  drawBalls() {
    let colorCodes = [...this.randomColors, ...this.allowedColors]
  
    for (let i = 0; i < this.columns; i++) {
      let colorsBalls = Array.from({length: this.rows}, (v, i) => 0);
      let random = 0;
      for (let j = 0; j < this.allowedColorsPerColumn; j++) {
        do {
          random = Math.floor(Math.random() * this.rows);
        } while (colorsBalls[random] != 0);
        colorsBalls[random] = this.randomColors.length + Math.floor(Math.random() * this.allowedColors.length);
      }
      for (let j = 0; j < this.rows; j++) {
        if (colorsBalls[j] == 0) {
          colorsBalls[j] = Math.floor(Math.random() * this.randomColors.length);
        }
      } 

      this.balls.push(Array.from({length: this.rows}, (v, j) => new Ball(intialOffsetBalls - gapBetweenBallsHorizondal * i, initialConveryerPosition + (ballRadius * 2 * j) + (gapBetweenBallsVertical * j), ballRadius, colorCodes[colorsBalls[j]].color, this.canvas.el, colorCodes[colorsBalls[j]].label)));
    }
  }

  drawBuckets = () => {
    this.allowedColors.forEach( (allowedColor, i) => {
      let evenBucketsOffset = (1 - (this.allowedColors.length % 2)) * (bucketGap * 0.5 + bucketWidth * 0.5);
      let bucketsOffset = 0;
      let porm = 1;
      if (i <= Math.floor((this.allowedColors.length - 1)/ 2)) {
        bucketsOffset = (Math.floor((this.allowedColors.length - 1)/ 2) - i) * (bucketGap + bucketWidth);
        porm = -1;
      } else {
        bucketsOffset = (i - Math.floor(this.allowedColors.length / 2)) * (bucketGap + bucketWidth);
      }
      let bucketX = ((fixedWidth - bucketWidth) / 2) + ((porm) * (evenBucketsOffset + bucketsOffset));
      let bucketY =  initialConveryerPosition + (ballRadius * 2 * (this.rows - 1)) + (gapBetweenBallsVertical * (this.rows - 1)) + bucketAndConveyerOffset;
      this.buckets.push(new Bucket(bucketX, bucketY, allowedColor.color, this.canvas.el, allowedColor.label, this.droppedABall));
    })
  }

  updateBalls() {
    for (let i = 0; i < this.columns; i++) {
      for (let j = 0; j < this.rows; j++) {
        this.balls[i][j].x += ballVX;
        this.balls[i][j].updateCoords();
      }
    }
  }
  
  play = () => {
    this.updateBalls();

    if (this.balls[this.columns - 1][0].x < fixedWidth + 20)
      requestAnimationFrame(this.play);
    else {
      console.log("level ended");
      this.statistics = this.calcStatistics();
      this.reset();
      window.dispatchEvent(this.completeEvent);
    }
  }

  calcStatistics = () => {
    
    let allowedLabels = this.allowedColors.map(allowedColor => allowedColor.label);

    this.buckets.forEach(bucket => {
      bucket.contents.forEach(ball => {
        ball.dropError = (ball.destinationReached && (ball.label != bucket.label))? 1: 0;
      });
    });

    this.balls.forEach(rows => {
      rows.forEach(ball => {
        ball.missingError = (ball.destinationReached || allowedLabels.indexOf(ball.label) == -1)? 0: 1;
      });
    });

    let droppedBalls = this.ballsDropped.map(ball => {
      return {
        dropError: ball.dropError,
        missingError: ball.missingError,
        actionTime: ball.actionTime,
        reactionTime: ball.reactionTime,
        distance: ball.distance,
        color: ball.label,
      };
    });

    let missingBalls = this.balls.reduce((totalArray, rows) => {
      return totalArray.concat(rows.filter(ball => (ball.missingError == 1)).map(ball => {
        return {
          dropError: ball.dropError,
          missingError: ball.missingError,
          actionTime: ball.actionTime,
          reactionTime: ball.reactionTime,
          distance: ball.distance,
          color: ball.label,
        };
      }));
    }, []);

    return droppedBalls.concat(missingBalls);
  }

  droppedABall = ball => {
    this.ballsDropped.push(ball);
  }

  reset() {
    this.canvas.clear();
    draggedElement = null;
    draggedStartTime = 0;
    draggedEndTime = 0;
  }
}

// globals dependent of levels
draggedElement = null;
draggedStartTime = 0;
draggedEndTime = 0;

// check if dimensions are within bounds
function checkScreenDimensions() {
  return !(fixedHeight > window.innerHeight || fixedWidth > window.innerWidth);
}

class Sound {
  constructor() {
    this.context = new AudioContext();
  }
  
  async play(frequency, duration, gain = 0.5, wave = "sine") {
    try {
      (this.context.state == "suspended") && await this.context.resume();
      let oscillator = this.context.createOscillator();
      let gainNode = this.context.createGain();
  
      oscillator.connect(gainNode);
      gainNode.connect(this.context.destination);
  
      gainNode.gain.setValueAtTime(gain, this.context.currentTime);
      
      oscillator.type = wave;
      oscillator.frequency.value = frequency;
  
      oscillator.start(this.context.currentTime);
      
      await timeout(duration, () => gainNode.gain.setValueAtTime(0, this.context.currentTime));
    } catch (err) {
      console.log(err);
    } 
  }

  async collect() {
    await this.play(440 * Math.pow(2, 10/12), 150, 0.5, "sawtooth");
    await timeout(30);
    this.play(440 * Math.pow(2, 15/12), 200, 0.5, "sawtooth");
  }

  async collectBad() {
    await this.play(440 * Math.pow(2, 3/12), 250, 0.5, "sawtooth");
  }
  
  async pattern() {
    let Cmaj = async () => {
      await this.play(440 * Math.pow(2, -9/12), 150, 0.1);
      await timeout(50);
      await this.play(440 * Math.pow(2, -5/12), 150, 0.1);
      await timeout(50);
      await this.play(440 * Math.pow(2, -2/12), 200, 0.1);
    }
    this.stopPattern = false;
    while (!this.stopPattern) {
      await Cmaj();
      await Cmaj();
      await Cmaj();
      await Cmaj();
      
      await this.play(440, 400, 0.1);
      await this.play(440 * Math.pow(2, -2/12), 400, 0.1);
      
      await Cmaj();
      await Cmaj();
      await Cmaj();
      await Cmaj();
      
      await this.play(440 * Math.pow(2, -4/12), 150, 0.1);
      await timeout(50);
      await this.play(440 * Math.pow(2, -5/12), 150, 0.1);
      await timeout(50);
      await this.play(440 * Math.pow(2, -2/12), 300, 0.1);
      await timeout(100);
    }

  }
}

function timeout(duration, fns = "") {
  return new Promise(resolve => {
    setTimeout(() => {
      fns && fns();
      resolve();
    }, duration);
  });
}